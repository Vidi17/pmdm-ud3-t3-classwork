package dam.android.vidal.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import dam.android.vidal.u3t3menuofactivities.model.Item;

public class ItemDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView image;
    private TextView version;
    private TextView versionName;
    private TextView dateItem;
    private TextView apiNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Item item = (Item) getIntent().getSerializableExtra("item");
        this.setTitle(item.getVersionName());

        image = findViewById(R.id.imagenAndroid);
        version = findViewById(R.id.versionNumber);
        versionName = findViewById(R.id.versionNameItem);
        dateItem = findViewById(R.id.dateItem);
        apiNumber = findViewById(R.id.apiNumberItem);

        image.setImageResource(item.getImageId());
        image.setClickable(true);

        version.setText("Version: " + item.getVersion());
        versionName.setText(item.getVersionName());
        dateItem.setText(String.format("%d", item.getReleaseDate().getYear()));
        apiNumber.setText("API: " + item.getAPInumber());

        image.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.setData(Uri.parse(item.getWikiUrl()));
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onClick(View v) {

    }
}