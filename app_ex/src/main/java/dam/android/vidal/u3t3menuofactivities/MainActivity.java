package dam.android.vidal.u3t3menuofactivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;

import dam.android.vidal.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private  RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView imageView;
    private Button btAdd, btDelete, btRestore;

    //TODO EX2
    private final ArrayList<Item> myDataSet = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        fillArray(myDataSet);
        imageView = findViewById(R.id.ivVacio);
        imageView.setVisibility(View.INVISIBLE);

        btAdd = findViewById(R.id.btAdd);
        btDelete = findViewById(R.id.btDeleteAll);
        btRestore = findViewById(R.id.btRestore);

        btAdd.setOnClickListener(this);
        btDelete.setOnClickListener(this);
        btRestore.setOnClickListener(this);

        recyclerView = findViewById(R.id.recyclerViewActivities);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyAdapter(myDataSet, this);
        new ItemTouchHelper(itemTouch).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(mAdapter);
    }

    //TODO EX1
    @Override
    public void onItemClick(int position, String activityName) {
        Toast.makeText(this,activityName, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, ItemDetailActivity.class);
        intent.putExtra("item", myDataSet.get(position));
        startActivity(intent);
    }

    //TODO EX2
    private void fillArray(ArrayList<Item> dataSet) {
        Date releaseDate12 = new Date(2021, 2, 18);
        Date releaseDate11 = new Date(2020, 9, 8);
        Date releaseDate10 = new Date(2019, 9, 3);
        Date releaseDate9 = new Date(2018, 8, 6);
        Date releaseDate8 = new Date(2017, 8, 21);
        Date releaseDate7 = new Date(2016, 8, 22);
        Date releaseDate6 = new Date(2015, 10, 2);
        Date releaseDate5 = new Date(2014, 11, 4);

        Item android12 = new Item(R.drawable.android12
                , 12, "Android 12", releaseDate12, 31
                , "https://es.wikipedia.org/wiki/Android_12");
        dataSet.add(android12);

        Item android11 = new Item(R.drawable.android11
                , 11, "Android 11", releaseDate11, 30
                , "https://es.wikipedia.org/wiki/Android_11");
        dataSet.add(android11);

        Item android10 = new Item(R.drawable.android10
                , 10, "Android 10", releaseDate10, 29
                , "https://es.wikipedia.org/wiki/Android_10");
        dataSet.add(android10);

        Item android9 = new Item(R.drawable.android9
                , 9, "Android Pie", releaseDate9, 28
                , "https://es.wikipedia.org/wiki/Android_Pie");
        dataSet.add(android9);

        Item android8 = new Item(R.drawable.android8
                , 8, "Android Oreo", releaseDate8, 26
                , "https://es.wikipedia.org/wiki/Android_Oreo");
        dataSet.add(android8);

        Item android7 = new Item(R.drawable.android7
                , 7, "Android Nougat", releaseDate7, 25
                , "https://es.wikipedia.org/wiki/Android_Nougat");
        dataSet.add(android7);

        Item android6 = new Item(R.drawable.android6
                , 6, "Android Marshmallow", releaseDate6, 23
                , "https://es.wikipedia.org/wiki/Android_Marshmallow");
        dataSet.add(android6);

        Item android5 = new Item(R.drawable.android5
                , 5, "Android Lollipop", releaseDate5, 22
                , "https://es.wikipedia.org/wiki/Android_Lollipop");
        dataSet.add(android5);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAdd:
                Item newItem = new Item();
                myDataSet.add(newItem);
                mAdapter.notifyDataSetChanged();
                recyclerView.smoothScrollToPosition(myDataSet.size());
                imageView.setVisibility(View.INVISIBLE);
                break;
            case R.id.btDeleteAll:
                myDataSet.clear();
                mAdapter.notifyDataSetChanged();
                imageView.setVisibility(View.VISIBLE);
                break;
            case R.id.btRestore:
                myDataSet.clear();
                fillArray(myDataSet);
                mAdapter.notifyDataSetChanged();
                imageView.setVisibility(View.INVISIBLE);
                break;
        }
    }

    ItemTouchHelper.SimpleCallback itemTouch = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }


        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            myDataSet.remove(viewHolder.getAdapterPosition());
            mAdapter.notifyDataSetChanged();

            if (myDataSet.isEmpty()){
                imageView.setVisibility(View.VISIBLE);
            }else {
                imageView.setVisibility(View.INVISIBLE);
            }
        }
    };
}