package dam.android.vidal.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.vidal.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    public interface OnItemClickListener{
        void onItemClick(int position, String activityName);
    }

    private ArrayList<Item> myDataSet;
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imagen;
        public TextView version;
        public TextView name;
        public TextView apiNumber;

        public MyViewHolder(View view) {
            super(view);
            imagen = view.findViewById(R.id.imagen);
            name = view.findViewById(R.id.versionName);
            version = view.findViewById(R.id.version);
            apiNumber = view.findViewById(R.id.apiNumber);
        }
        public void bind(String activityName, OnItemClickListener listener){
            this.itemView.setOnClickListener(v -> listener.onItemClick(getAdapterPosition(), activityName));
        }
    }

    public MyAdapter(ArrayList<Item> myDataSet, OnItemClickListener listener) {
        this.myDataSet = myDataSet;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.imagen.setImageResource(myDataSet.get(position).getImageId());
        viewHolder.apiNumber.setText(String.format("API %d", myDataSet.get(position).getAPInumber()));
        viewHolder.version.setText(String.format("Version: %d", myDataSet.get(position).getVersion()));
        viewHolder.name.setText(myDataSet.get(position).getVersionName());
        viewHolder.bind(myDataSet.get(position).getVersionName(), listener);
        viewHolder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}
