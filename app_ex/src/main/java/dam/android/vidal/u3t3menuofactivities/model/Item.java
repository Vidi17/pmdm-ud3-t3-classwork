package dam.android.vidal.u3t3menuofactivities.model;

import java.io.Serializable;
import java.util.Date;

import dam.android.vidal.u3t3menuofactivities.R;

//TODO EX2
public class Item implements Serializable {

    private int imageId;
    private int version;
    private String versionName;
    private Date releaseDate;
    private int APInumber;
    private String wikiUrl;

    public Item(int imageId, int version, String versionName, Date releaseDate, int APInumber, String wikiUrl) {
        this.imageId = imageId;
        this.version = version;
        this.versionName = versionName;
        this.releaseDate = releaseDate;
        this.APInumber = APInumber;
        this.wikiUrl = wikiUrl;
    }

    public Item() {
        imageId = R.drawable.mosquitoe;
        version = 13;
        versionName = "Android Mosquitoe";
        Date date = new Date();
        date.setYear(2022);
        releaseDate = date;
        APInumber = 32;
        wikiUrl = "https://es.wikipedia.org/wiki/Android";
    }

    public int getImageId() {
        return imageId;
    }

    public int getVersion() {
        return version;
    }

    public String getVersionName() {
        return versionName;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public int getAPInumber() {
        return APInumber;
    }

    public String getWikiUrl() {
        return wikiUrl;
    }
}
